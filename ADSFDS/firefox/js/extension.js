window.onload = function(){
	var isRequestFromMOEnduserConsole = window.location.href.match(/^https?:\/\/([a-zA-Z\d-]+\.){0,}miniorange\.com\/moas\/enduserwelcome/) || window.location.href.match(/^https?:\/\/([a-zA-Z\d-]+\.){0,}miniorange\.in\/moas\/enduserwelcome/) || window.location.href.match(/http:\/\/localhost\:([0-9]){4}\/moas\/enduserwelcome/);
	var isRequestFromMO = window.location.href.match(/moas/g);
	if(isRequestFromMOEnduserConsole != null) {	//check for enduser console on localhost & all MO subdomains
		//adding div tag to body of webpage
		//alert('putting div');
		//console.log('putting div');
		var isInstalledNode = document.createElement('div');
		isInstalledNode.id = 'firefox-extension-is-installed';
		document.body.appendChild(isInstalledNode);
		
		//getting version of the extension
		var currExtVersion = document.createElement('input');
		currExtVersion.id = 'old-version';
		currExtVersion.type = 'hidden';
		currExtVersion.value = getVersion();
		console.log('current extension version: ' + getVersion())
		document.body.appendChild(currExtVersion);
		return;
	} else if(isRequestFromMO != null) {	//check if page from MO
		//dont do anything
		return;
	} else {
		//check if the extension should work on the current website - for all other websites except the ones given above
		var sessionId = getParameterByName("ssc");
		var sessionIdFromHidden = document.getElementsByName("ssc")[0];
		var sessionIdFromCookie = getCookie("ssc");
		if((sessionId != '' && sessionId != null) || (sessionIdFromCookie != '' && sessionIdFromCookie != null) || (sessionIdFromHidden != '' && sessionIdFromHidden != null)) {
			loginToSSOApp();
		}
		return;
	}
}