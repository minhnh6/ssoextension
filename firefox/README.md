This extension lets users SSO into applications through miniOrange. This works for all applications which do not support SSO standard SAML protocol.

The user needs to be logged into miniOrange to use this extension.