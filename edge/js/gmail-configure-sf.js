//window.onload = configure;

function configureGoogleSF() {
	//alert("in gmail configure");
	var currentUrl = document.URL;
	var configursf = getCookie("configuresf");
	if(currentUrl.indexOf("mail") > -1) {
		//alert("mail");
		//alert(document.cookie);
		var configursf = getCookie("configuresf");
		//alert("configured:" + configursf);
		if(configursf == "true") {
			window.location.href="https://accounts.google.com/b/0/SmsAuthSettings#devices";
		}
	} else if(configursf == "true") {
		//alert("configured:" + configursf);
		if(currentUrl.indexOf("SmsAuthSettings") > -1) {
			//alert("SmsAuthSettings");
			//alert("configured:" + configured);
			//alert("sf settings");
			var chooseMoveToDifferentPhone = document.getElementById('smsauth3settings-chooseapp-button');
			//alert(chooseMoveToDifferentPhone);
			if(chooseMoveToDifferentPhone != null) {
				//alert("chooseMoveToDifferentPhone");
				chooseMoveToDifferentPhone.click();
				//alert("clicked");
				var chooseAppType = document.getElementById("smsauth3settings-choose-app-type-radio-android");
				//alert(chooseAppType);
				chooseAppType.checked = true;
				var chooseContinue = document.getElementsByName("action");
				var text = "";
				//alert(text);
				//alert(chooseContinue.length);
				chooseContinue[2].click();
				setTimeout(function(){
					var androidKey = $('#smsauth3settings-configure-app-android-key').text();
					//alert(androidKey);
					//alert(document.cookie);
					var sessionId = getCookie('ssc');
					var appId = getCookie("appId");
					var env = getCookie("envType");
					var serviceEndpoint = getSaveSecretServiceEndPoint(env);
					//alert(serviceEndpoint);
					var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\",\"secretKey\":\"" + androidKey + "\"}";
					//alert(jsonString);
					$.ajax({
						url : serviceEndpoint,
						type : "POST",
						dataType : "json",
						data : jsonString,
						contentType : "application/json; charset=utf-8",
						success : function(result){
							var jsonObj = JSON.parse(JSON.stringify(result));
							//alert(JSON.stringify(result));
							var status = jsonObj.status;
							//alert("status: " + status);
							var secondFactor = jsonObj.secondFactor;
							//alert("second factor" + secondFactor);

							if(status == 'SUCCESS'){
								//alert("in success");
								removeExtraParamsFromUrl(jsonString);
								var secondFactorField = document.getElementById('smsauth3settings-configure-app-android-input');
								secondFactorField.value = secondFactor;
								var verifyKeySubmit = document.getElementById('smsauth3settings-configure-app-android-verify');
								verifyKeySubmit.disabled = false;
								verifyKeySubmit.click();
								var okButtons = document.getElementsByName("ok");
								//alert(okButtons.length);
								okButtons[1].click();
								document.cookie = "ssc=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
								document.cookie = "appId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
								document.cookie = "envType=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/"
								document.cookie = "configuresf=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
								window.location.href = getReturnEndPoint(env);
							}
							else if(status == 'ERROR'){
								console.log('Status: ERROR');
								return null;
							}

						},
						error: function(error) {
							console.log("error");
							console.log(error[0] + error[1]);
						}
					});
				}, 1000);
				var cantScanLink = document.getElementById("smsauth3settings-configure-app-android-manual-zippy");
				cantScanLink.click();
			} else {
				//alert("choose phone null");
				var step3Element = document.getElementById("step-3");
				if(step3Element.className != "selected") {
					setTimeout(configure, 2000);
				} else {
					//alert("step3selected");
					var rememberCheckbox = document.getElementById("remember-checkbox");
					var nextButton = document.getElementById("next-button");
					rememberCheckbox.checked = false;
					nextButton.click();
					var step4Element = document.getElementById("step-4");
					if(step4Element.className == "selected")
						nextButton.click();
				}
								
				//alert(step4Element);
				//alert(step4Element.className);
				//rememberCheckbox.checked = false;
				//nextButton.click();
				//alert("remember: "+rememberCheckbox);
				//setTimeout("configure()", 2000);
				//if(
			}
			//var androidKey = $('#smsauth3settings-configure-app-android-key').text();
			//alert(androidKey);
			//alert();
		} else if(currentUrl.indexOf("SmsAuthLanding") > -1) {
			var setupbutton = document.getElementById("setupbutton");
			setupbutton.click();
			//setTimeout(
		}
	}  
}

function getSaveSecretServiceEndPoint(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/save-2f-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/save-2f-details";
	} else if (env == 'test') {
		serviceEndpoint = "https://test.miniorange.in/moas/rest/extension/save-2f-details";
	} else if (env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/save-2f-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/save-2f-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/save-2f-details";
	}
	return serviceEndpoint;
}

function getReturnEndPoint(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/";
	} else if (env == 'test') {
		serviceEndpoint = "https://test.miniorange.in/moas/";
	} else if (env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/";
	}
	return serviceEndpoint;
}