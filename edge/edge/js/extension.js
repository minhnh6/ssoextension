window.onload = function(){
	var isRequestFromMOEnduserConsole = window.location.href.match(/^https?:\/\/([a-zA-Z\d-]+\.){0,}miniorange\.(com|in)\/moas\/enduserwelcome/) || window.location.href.match(/http:\/\/localhost\:([0-9]){4}\/moas\/enduserwelcome/);
	var isRequestFromMO = window.location.href.match(/moas/g);
	var isRequestFromGoogleSF = window.location.href.match(/accounts.google.com\/b\/0\/SmsAuthSettings/) || window.location.href.match(/mail.google.com\/mail/) || window.location.href.match(/accounts.google.com\/b\/0\/SmsAuthLanding/);
	console.log("isRequestFromMOEnduserConsole " + isRequestFromMOEnduserConsole);
	if(isRequestFromMOEnduserConsole != null) {	//check for enduser console on localhost & all MO subdomains
		//adding div tag to body of webpage
		//alert("added div");
		var isInstalledNode = document.createElement('div');
		isInstalledNode.id = 'extension-is-installed';
		document.body.appendChild(isInstalledNode);
		
		//getting version of the extension
		var currExtVersion = document.createElement('input');
		currExtVersion.id = 'old-version';
		currExtVersion.type = 'hidden';
		currExtVersion.value = getVersion();
		document.body.appendChild(currExtVersion);
		return;
	} else if(isRequestFromMO != null) {	//check if page from MO
		//dont do anything
		return;
	} else if(isRequestFromGoogleSF != null) {	//check if google second factor pages
		//matches google second factor configuration URL
		configureGoogleSF();
		return;
	} else {
		//check if the extension should work on the current website - for all other websites except the ones given above
		var sessionId = getParameterByName("ssc");
		var sessionIdFromHidden = document.getElementsByName("ssc")[0];
		var sessionIdFromCookie = getCookie("ssc");
		if((sessionId != '' && sessionId != null) || (sessionIdFromCookie != '' && sessionIdFromCookie != null) || (sessionIdFromHidden != '' && sessionIdFromHidden != null)) {
			loginToSSOApp();
		}
		return;
	}
}