function loginToSSOApp() {
	//alert("in app-field");
	var sessionId = getParameterByName("ssc");
	var appId = getParameterByName("appId");
	if(!appId){
		appId = getParameterByName("applicationId");
	}
	var envType = getParameterByName("env");
	
	//check if there is another login page for the app
	var nxtpage = getCookie("nxtpage");
	var pageNo = 1;
	if(nxtpage == "true") {
		pageNo = 2;
	}
	//alert("pageno: " + pageNo);

	if(sessionId == '' || appId == '' || envType == '') {
		//if parameters do no exist, the data maybe stored in cookies
		sessionId = getCookie("ssc");
		appId = getCookie("appId");
		envType = getCookie("envType");
	}

	if(sessionId == '' || appId == '' || envType == '') {
		//if parameters and cookies do no exist, the data maybe stored in hidden fields
		sessionId = document.getElementsByName("ssc")[0].value;
		appId = document.getElementsByName("appId")[0].value;
		envType = document.getElementsByName("env")[0].value;
	}
	
	var serviceEndpoint = getServiceEndpointForLogin(envType);
	//alert("envType: " + envType + "   serviceEndpoint: " + serviceEndpoint);
	var ssoParameterJson = "{\"applicationId\":\"" + appId +"\",\"appLoginPageNumber\":"+ pageNo + ",\"sessionId\":\""+ sessionId + "\"}";
	console.log("jsonString: " + ssoParameterJson);
	console.log("serviceEndpoint"+serviceEndpoint);
	$.ajax({
			url: serviceEndpoint,
			type : "POST",
			dataType : "json",
			data : ssoParameterJson,
			contentType : "application/json; charset=utf-8",
			success : function(result) {
				// console.log("sending request");
				// console.log(JSON.stringify(result));
				var jsonObj = JSON.parse(JSON.stringify(result));

				var status = jsonObj.status;
				var appDomain =  jsonObj.applicationDomain;
				var appType = jsonObj.applicationType;
				var formField = jsonObj.formField;
				var usernameField = jsonObj.usernameField;
				var passwordField = jsonObj.passwordField;
				var buttonField = jsonObj.buttonField;
				var secondFactorField = jsonObj.secondFactorField;
				var domainField = jsonObj.domainField;
				var isMultiplePages = jsonObj.isMultiplePages;
				var preLoginClickField = jsonObj.preLoginClickField;
				
				if (status == 'SUCCESS') {
					//initialse object
					var currentApp = new AppField(appType, appDomain, formField, usernameField, passwordField, buttonField, secondFactorField, domainField, preLoginClickField, isMultiplePages);
					
					currentApp.performSSOByAppType(formField, usernameField, passwordField, buttonField);
				}
			}, 
			error: function(error){
				console.log("error: "+JSON.stringify(error));
				console.log("error: " + error[0] + error[1]);
			}
	});
}

//object declaration and initialisation
var AppField = function(appType, appDomain, form, user, pass, button, secondFactor, domainName, preLoginClick, multiplePages) {
	this.appType = appType;
	this.appDomain = appDomain;
	this.formField = performEval(form);
	this.userField = performEval(user);
	this.passField = performEval(pass);
	this.buttonField = performEval(button);
	this.clickButton = doButtonClick(this.buttonField);
	this.secondFactorField = performEval(secondFactor);
	this.domainField = performEval(domainName);
	this.preLoginClickField = performEval(preLoginClick);
	this.isMultiplePages = multiplePages;
}

AppField.prototype.performSSOByAppType = function(formField, usernameField, passwordField, buttonField) {
	if(this.appType == 'SIMPLE_LOGIN' || this.appType == 'LOGIN_WITH_DOMAIN' || this.appType == 'LOGIN_AFTER_CLICK') {
		if(this.isMultiplePages == "true") 
			document.cookie = "nxtpage=true; path=/; domain=." + this.appDomain + ".com";
		if(this.preLoginClickField !== null) {
			//the fields were not available before preLogin field click
			this.preLoginClickField.click();
			this.formField = performEval(formField);
			this.userField = performEval(usernameField);
			this.passField = performEval(passwordField);
			this.buttonField = performEval(buttonField);
			this.clickButton = doButtonClick(this.buttonField);
		}
		performSSO(this.formField, this.userField, this.passField, this.buttonField, this.clickButton, false, this.appDomain, this.domainField);
		
	} else if(this.appType == 'LOGIN_SECOND_PAGE' || this.appType == 'LOGIN_WITH_SECONDFACTOR' || this.appType == 'LOGIN_WITH_SECONDFACTOR_GOOGLE') {
		performSSOPageTwo(this.formField, this.userField, this.passField, this.buttonField, this.clickButton, this.secondFactorField, this.appDomain);
	}
}

function doButtonClick(button) {
	if(button !== null) {
		return true;
	} else {
		return false;
	}
}

//checks if the field to be evaluated is valid
function performEval(loginFieldString) {
	var loginFieldElement = eval(loginFieldString);
	if(loginFieldElement != undefined && loginFieldElement !== null && (isNode(loginFieldElement) || isElement(loginFieldElement))) {
		return loginFieldElement;
	} else {
		return null;
	}
}

//Returns true if it is a DOM node
function isNode(o){
  return (
    typeof Node === "object" ? o instanceof Node : 
    o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
  );
}

//Returns true if it is a DOM element    
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName==="string"
);
}

function getServiceEndpointForLogin(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/login-field-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/login-field-details";
	} else if(env == 'test') {
		serviceEndpoint = "https://test.miniorange.in/moas/rest/extension/login-field-details";
	} else if(env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/login-field-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/login-field-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/login-field-details";
	}

	return serviceEndpoint;
}