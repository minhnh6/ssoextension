function performSSO(loginForm, loginField, passwordField, buttonField, clickButton, customStep, appDomain, domainField){
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);

	var sessionId = getParameterByName("ssc");
	//alert(sessionId);
	var appId = getParameterByName("appId");
	//alert("appid is: " + appId);
	
	if(!appId){
		//alert("appId inside null");
		appId = getParameterByName("applicationId");
	}
	var envType = getParameterByName("env");
	//alert(envType);
	var configuresf = getParameterByName("configuresf");
	//alert(configuresf);

	if(sessionId == '' || appId == '' || envType == '') {
		//if parameters and cookies do no exist, the data maybe stored in hidden fields
		sessionId = document.getElementsByName("ssc")[0].value;
		appId = document.getElementsByName("appId")[0].value;
		envType = document.getElementsByName("env")[0].value;
	}

	var serviceEndpoint = getServiceEndpoint(envType);
	//alert("serviceEndpoint " + serviceEndpoint);
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	//alert(jsonString);
	$.ajax({
			url: serviceEndpoint,
			type : "POST",
			dataType : "json",
			data : jsonString,
			contentType : "application/json; charset=utf-8",
			success : function(result) {
				// console.log("sending request in common");
				// console.log(JSON.stringify(result));
				var jsonObj = JSON.parse(JSON.stringify(result));

				var status =  jsonObj.status;
				var myUsername = decrypt(jsonObj.username, sessionId);
				var domainName = jsonObj.domainName == null? '' : decrypt(jsonObj.domainName, sessionId);
				var myPassword = decrypt(jsonObj.password, sessionId);


				if (status == 'SUCCESS') {
					configureForSecondLoginPage(appDomain, sessionId, appId, envType, configuresf);
					removeExtraParamsFromUrl(jsonString);
					// fill in your username, password and/or domain
					populateFieldWithValue(loginField, myUsername);
					populateFieldWithValue(passwordField, myPassword);
					populateFieldWithValue(domainField, domainName);
					
					// automaticaly submit the login form
					if (buttonField != null && clickButton == true) {
						//alert("button clicked");
						buttonField.click();
						if (customStep == true){
                        	setTimeout("location.reload();",2000);
						}
						return false;

					} else if (loginForm != null) {
						//alert("form submitted");
						loginForm.submit();
					}

				} else if (status == 'ERROR') {
					return null;
				}
			}, 
			error: function(error){
				console.log("error: " + error);
			}
	});

}

//login to second login page of app
function performSSOPageTwo(loginForm, loginField, passwordField, buttonField, clickButton, secondFactorField, appDomain){
	//alert('Password page');
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);
	var sessionId = getCookie("ssc");
	//console.log(sessionId);
	var appId = getCookie("appId");
	var env = getCookie("envType");
	var currentLocation = window.location.href;
	var serviceEndpoint;
	serviceEndpoint = getServiceEndpoint(env);
	//alert("serviceEndpoint: " + serviceEndpoint);
	var regex = new RegExp("\/*" + appDomain + "*");
	if(currentLocation.match(regex) && getCookie("nxtpage") == true && secondFactorField !== null) {
		serviceEndpoint = getServiceEndPointFor2F(env);
	}

	//alert("SSC:" + sessionId + " AppId:" + appId + " Env:" + env);
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	//alert(jsonString);

	//Send REST service call to service end point to get soft token
	$.ajax({
		url : serviceEndpoint,
		type : "POST",
		dataType : "json",
		data : jsonString,
		contentType : "application/json; charset=utf-8",
		success : function(result){
			var jsonObj = JSON.parse(JSON.stringify(result));
			//alert("jsonObj: " + JSON.stringify(result));
			var status = jsonObj.status;
			var myUsername = decrypt(jsonObj.username, sessionId);
			var myPassword = decrypt(jsonObj.password, sessionId);
			var secondFactor = decrypt(jsonObj.secondFactor, sessionId);
			//alert("status: " + status);
			//alert("username " + myUsername);
			//alert("password" + myPassword);
						
			if(status == 'SUCCESS'){
				if(appDomain == 'google') {
					//check if second factor needs to be configured
					var configuresf = getCookie("configuresf");
					if(configuresf != "true") {
						removeAllCookies(appDomain);
					}
				} else {
					//delete cookies for all two-page login apps
					removeAllCookies(appDomain);
				}
				
				populateFieldWithValue(loginField, myUsername);
				populateFieldWithValue(passwordField, myPassword);
				populateFieldWithValue(secondFactorField, secondFactor);
				
				if (buttonField != null && clickButton == true) {
					//alert("button clicked");
					buttonField.click();
					return false;
				} else if (loginForm != null) {
						//alert("form submitted");
						loginForm.submit();
				}
			}
			else if(status == 'ERROR'){
				//alert('Status: ERROR');
				return null;
			}
		}
	});
}

function populateFieldWithValue(field, value) {
	if(field != null) {
		field.focus();
		field.value = value;
		field.blur();
	}
}

function configureForSecondLoginPage(appDomain, sessionId, appId, envType, configuresf) {
	var currentLocation = window.location.href;
	//alert("current location " + currentLocation);
	var regex;
	var checkNextPage = getCookie("nxtpage");
	//alert("checkNextPage: " + checkNextPage);
	if(appDomain != 'google') {
		regex = new RegExp("\/*" + appDomain + "*");
		//alert("regex: " + regex);
	}
	
	//specific case for google
	if(currentLocation.match(/\/*google*/)) {
		document.cookie = "ssc="+sessionId+"; path=/; domain=.google.com";
		document.cookie = "appId="+appId+"; path=/; domain=.google.com";
		document.cookie = "envType="+envType+"; path=/; domain=.google.com";
		if(configuresf != "true")
			document.cookie = "configuresf=false; path=/; domain=.google.com";
		else
			document.cookie = "configuresf=true; path=/; domain=.google.com";
	} 
	//for apps having two login pages
	else if(currentLocation.match(regex) && checkNextPage == "true") {
		//alert("matches " + appDomain);
		//var host = window.location.host.split(".");
		document.cookie = "ssc="+sessionId+"; path=/;domain=." + appDomain + ".com";
		document.cookie = "appId="+appId+"; path=/;domain=." + appDomain + ".com";
		document.cookie = "envType="+envType+"; path=/;domain=." + appDomain + ".com";
	}
}

//Get parameters by name from the url
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	//location.search would be null if there is # in the URL
	if(location.search == "") {
		var searchString = getParametersFromUrl();
		results = regex.exec(searchString);
	} else {
		results = regex.exec(location.search);
	}
    return results == null ? "" : results[1].replace(/\+/g, " ");
}

function getParametersFromUrl() {
	var url = window.location.href;
	var urlPart = url.split("?");
	var searchString = "?" + urlPart[1];
	return searchString;
}

//repeated 3 times!!
function getServiceEndpoint(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/sign-in-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/sign-in-details";
	} else if(env == 'test') {
		serviceEndpoint = "https://test.miniorange.in/moas/rest/extension/sign-in-details";
	} else if(env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/sign-in-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/sign-in-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/sign-in-details";
	}

	return serviceEndpoint;
}

function getServiceEndPointFor2F(env){
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/get-2f-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/get-2f-details";
	} else if(env == 'test') {
		serviceEndpoint = "https://test.miniorange.in/moas/rest/extension/get-2f-details";
	} else if(env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/get-2f-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/get-2f-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/get-2f-details";
	}
	return serviceEndpoint;
}

//remove query string parameters from current url
function removeExtraParamsFromUrl(jsonString) {
	var currentLocation = window.location.href;
	//alert("current location " + currentLocation);
	var indexOfSsc = currentLocation.indexOf("&ssc=");
	var indexOfSsc2 = currentLocation.indexOf("?ssc=");
	//alert("index " + indexOfSsc);
	if (indexOfSsc != -1) {
		currentLocation = currentLocation.substring(0,indexOfSsc);
	} else if (indexOfSsc2 != -1) {
		currentLocation = currentLocation.substring(0,indexOfSsc2);
	}
	//alert("current location changed " + currentLocation);
	window.history.pushState(jsonString, window.title, currentLocation);
}

function removeAllCookies(appDomain) {
	document.cookie = "ssc=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=." + appDomain + ".com";
	document.cookie = "appId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=." + appDomain + ".com";
	document.cookie = "envType=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=." + appDomain + ".com";
	document.cookie = "nxtpage=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=." + appDomain + ".com";
	document.cookie = "configuresf=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

function decrypt(ciphertext, key) {
	var iv = '02f30dffbb0d084755f438f7d8be4a7d';
	var encryptedData = atob(ciphertext);
	var decryptedData = mcrypt.Decrypt(encryptedData, iv, key, 'rijndael-128', 'ecb');
	var data = decryptedData.replace(/[\x00-\x1F\x7F\x81\x8D\x8F\x90\x9D\xA0\xAD]/g, '');
	return data;
}