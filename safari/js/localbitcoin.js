//login to LocalBitcoins
window.onload = function(){
	//alert("in localbitcoins");
	var loginForm = document.forms[0];
	var loginField = document.getElementById('id_username');
	var passwordField = document.getElementById('id_password');
	var secondFactorField = document.getElementById('id_token');

	if(secondFactorField == null){
		performSSO(loginForm, loginField, passwordField);
	}
	else{
		var userName = loginField.value;
		performSecondFactor(loginForm, userName, secondFactorField);
	}	
}