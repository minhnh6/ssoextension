function performSSO(loginForm, loginField, passwordField, buttonField, clickButton, checkReload, domainField){
	//alert("inside performSSO");
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);

	var sessionId = getParameterByName("ssc");
	//alert(sessionId);
	var appId = getParameterByName("appId");
	//alert(appId);
	
	if(!appId){
		//alert("appId inside null");
		appId = getParameterByName("applicationId");
	}
	
	var envType = getParameterByName("env");
	//alert(envType);
	var configuresf = getParameterByName("configuresf");
     //alert(configuresf);
     if(sessionId=='' || appId == '' ) {
         //alert('invalid session');
         return;
     }
     
	var serviceEndpoint = getServiceEndpoint(envType);
	alert("serviceEndpoint " + serviceEndpoint);
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	//alert(jsonString);
	$.ajax({
			url: serviceEndpoint,
			type : "POST",
			dataType : "json",
			data : jsonString,
			contentType : "application/json; charset=utf-8",
			success : function(result) {
				//alert(JSON.stringify(result));
				var jsonObj = JSON.parse(JSON.stringify(result));

				var status =  jsonObj.status;
				var myUsername = jsonObj.username;
				var myPassword = jsonObj.password;
				var domainName = jsonObj.domainName;
				alert("username " + myUsername);
				//alert("password " + myPassword);
				alert("status " + status);

				if (status == 'SUCCESS') {
					//alert("here " + jsonObj);
					// fill in your username and password
					var currentLocation = window.location.href;
					//alert("current location " + currentLocation);
					
					if(currentLocation.match(/\/*localbitcoins*/))
						console.log("LOCAL BITCOIN");
					else{
						removeExtraParamsFromUrl(jsonString);
					}
					if(currentLocation.match(/\/*google*/) || currentLocation.match(/\/*localbitcoins*/)) {
						document.cookie = "ssc="+sessionId+"; path=/; domain=.google.com";
						document.cookie = "appId="+appId+"; path=/; domain=.google.com";
						document.cookie = "envType="+envType+"; path=/; domain=.google.com";
						if(configuresf != "true")
							document.cookie = "configuresf=false; path=/; domain=.google.com";
						else
							document.cookie = "configuresf=true; path=/; domain=.google.com";
					} else if(currentLocation.match(/\/*unionbank*/)) {
						//alert("matches Union bank");
						document.cookie = "ssc="+sessionId+"; path=/; domain=.unionbank.com";
						document.cookie = "appId="+appId+"; path=/; domain=.unionbank.com";
						document.cookie = "envType="+envType+"; path=/; domain=.unionbank.com";
					}
					//focus() and blur() for Squarespace
					if(loginField != null) {
						loginField.focus();
						loginField.value = myUsername;
						loginField.blur();
					}
					//loginField.focus();
					//loginField.value = myUsername;
					if(passwordField != null) {
						passwordField.focus();
						passwordField.value = myPassword;
						passwordField.blur();
					}
					if(domainField != null) {
						domainField.value = domainName;
					}
					
					// automatically submit the login form
					if (buttonField != null && clickButton == true) {
						//alert("button clicked");
						var evObj = document.createEvent('MouseEvents');
						evObj.initMouseEvent('click', true, true, window);
						buttonField.dispatchEvent(evObj);
						if(checkReload) {
							setTimeout("location.reload(true)",2000);
						}
						return false;

					} else if (loginForm != null) {
						//alert("form submitted");
						loginForm.submit();
					}

				} else if (status == 'ERROR') {
					return null;
				}
			}
	});

}

//Get parameters by name from the url
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : results[1].replace(/\+/g, " ");
}

function getServiceEndpoint(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/sign-in-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/sign-in-details";
	} else if(env == 'test') {
		serviceEndpoint = "https://test.miniorange.com/moas/rest/extension/sign-in-details";
	} else if(env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/sign-in-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/sign-in-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/sign-in-details";
	}

	return serviceEndpoint;
}

function getServiceEndPointFor2F(env) {
	var serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/get-2f-details";
	if (env == 'prod') {
		serviceEndpoint = "https://auth.miniorange.com/moas/rest/extension/get-2f-details";
	} else if(env == 'test') {
		serviceEndpoint = "https://test.miniorange.com/moas/rest/extension/get-2f-details";
	} else if(env == 'uxd') {
		serviceEndpoint = "https://demo.miniorange.com/moas/rest/extension/get-2f-details";
	} else if (env == 'idp') {
		serviceEndpoint = "https://auth.miniorange.com/idp/moas/rest/extension/get-2f-details";
	} else if (env == 'local') {
		serviceEndpoint = "http://localhost:8080/moas/rest/extension/get-2f-details";
	}
	return serviceEndpoint;
}

//remove query string parameters from current url
function removeExtraParamsFromUrl(jsonString) {
	var currentLocation = window.location.href;
	//alert("current location " + currentLocation);
	var indexOfSsc = currentLocation.indexOf("&ssc=");
	var indexOfSsc2 = currentLocation.indexOf("?ssc=");
	//alert("index " + indexOfSsc);
	if (indexOfSsc != -1) {
		currentLocation = currentLocation.substring(0,indexOfSsc);
	} else if (indexOfSsc2 != -1) {
		currentLocation = currentLocation.substring(0,indexOfSsc2);
	}
	//alert("current location changed " + currentLocation);
	window.history.pushState(jsonString, window.title, currentLocation);
}

function performSecondFactor(loginForm, userName, secondFactorField){
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);
	var sessionId = getParameterByName("ssc");
	console.log(sessionId);
	var appId = getParameterByName("appId");
	var env = getParameterByName("env");
	var serviceEndpoint = getServiceEndPointFor2F(env);
	
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	console.log(jsonString);
	
	//Send REST service call to service end point to get soft token
	$.ajax({
		url : serviceEndpoint,
		type : "POST",
		dataType : "json",
		data : jsonString,
		contentType : "application/json; charset=utf-8",
		success : function(result){
			var jsonObj = JSON.parse(JSON.stringify(result));
			var status = jsonObj.status;
			console.log("status: " + status);
			var secondFactor = jsonObj.secondFactor;
			console.log("second factor" + secondFactor);
			
			if(status == 'SUCCESS'){
				removeExtraParamsFromUrl(jsonString);
				
				secondFactorField.value = secondFactor;
				loginForm.submit();
			}
			else if(status == 'ERROR'){
				console.log('Status: ERROR');
				return null;
			}
			
		}
	});
}

function performSecondFactorGmail(loginForm, secondFactorField){
	//alert('Gmail 2F');
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);
	var sessionId = getCookie("ssc");
	var appId = getCookie("appId");
	var env = getCookie("envType");
	var serviceEndpoint = getServiceEndPointFor2F(env);
	//alert("SSC:" + sessionId + "AppId:" + appId + "Env:" + env);
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	//alert(jsonString);

	//Send REST service call to service end point to get soft token
	$.ajax({
		url : serviceEndpoint,
		type : "POST",
		dataType : "json",
		data : jsonString,
		contentType : "application/json; charset=utf-8",
		success : function(result){
			var jsonObj = JSON.parse(JSON.stringify(result));
			var status = jsonObj.status;
			//alert("status: " + status);
			var secondFactor = jsonObj.secondFactor;
			//alert("second factor" + secondFactor);
			var configuresf = getCookie("configuresf");
			if(configuresf != "true") {
				document.cookie = "ssc=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
				document.cookie = "appId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
				document.cookie = "envType=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
				document.cookie = "configuresf=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.google.com; path=/";
			}
			if(status == 'SUCCESS'){
				document.getElementById("PersistentCookie").checked = false;
				secondFactorField.value = secondFactor;
				loginForm.submit();
			}
			else if(status == 'ERROR'){
				//alert('Status: ERROR');
				return null;
			}

		}
	});
}

function performSSOPassPage(loginForm, passwordField, buttonField, clickButton){
	//alert('Password page');
	var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
	//alert(url);
	var sessionId = getCookie("ssc");
	var appId = getCookie("appId");
	var env = getCookie("envType");
	var serviceEndpoint = getServiceEndpoint(env);
	//alert("SSC:" + sessionId + " AppId:" + appId + " Env:" + env);
	var jsonString = "{\"applicationId\":\""+appId+"\",\"sessionId\":\""+ sessionId + "\"}";
	//alert(jsonString);

	//Send REST service call to service end point to get soft token
	$.ajax({
		url : serviceEndpoint,
		type : "POST",
		dataType : "json",
		data : jsonString,
		contentType : "application/json; charset=utf-8",
		success : function(result){
			var jsonObj = JSON.parse(JSON.stringify(result));
			var status = jsonObj.status;
			//alert("status: " + status);
			var myUsername = jsonObj.username;
			var myPassword = jsonObj.password;
			//alert("username " + myUsername);
			//alert("password " + myPassword);
			
			document.cookie = "ssc=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.unionbank.com; path=/";
			document.cookie = "appId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.unionbank.com; path=/";
			document.cookie = "envType=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.unionbank.com; path=/";
			
			if(status == 'SUCCESS'){
				passwordField.focus();
				passwordField.value = myPassword;
				passwordField.blur();
				if (buttonField != null && clickButton == true) {
					//alert("button clicked");
					buttonField.click();
					return false;
				}
			}
			else if(status == 'ERROR'){
				//alert('Status: ERROR');
				return null;
			}
		}
	});
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}