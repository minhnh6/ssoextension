//login to marketo
window.onload = function(){
	var loginButton = document.getElementById('loginButton');
	var loginForm = document.getElementById('mktLogin');
	var loginField = document.getElementById('loginUsername');
	var passwordField = document.getElementById('loginPassword');
	//perform single sign on
	performSSO(loginForm, loginField, passwordField, loginButton, true);
}